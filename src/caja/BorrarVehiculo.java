/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caja;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import main.Cliente;
import Excepcion.DniNoExisteExcepcion;
import Excepcion.DocumentoInvalidoExcepcion;
import Excepcion.DominioNoExisteExcepcion;
import Excepcion.DoominioInvalidoExcepcion;
import Excepcion.ListaVaciaExcepcion;
import main.Playa;
import main.Vehiculo;
import Excepcion.VehiculoNoExisteExcepcion;

/**
 *
 * @author alumno
 */
public class BorrarVehiculo extends javax.swing.JPanel {

    /**
     * Creates new form BorrarVehiculo
     */
    Cliente nuevo;
    LinkedList<Vehiculo>listar = new LinkedList<Vehiculo>();
    public BorrarVehiculo() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldDNI = new javax.swing.JTextField();
        jTextFieldNombre = new javax.swing.JTextField();
        jTextFieldApellido = new javax.swing.JTextField();
        jTextFieldDominio = new javax.swing.JTextField();
        jTextFieldMarca = new javax.swing.JTextField();
        jTextFieldModelo = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jTextFieldVehiculo = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTablePatente = new javax.swing.JTable();

        setPreferredSize(new java.awt.Dimension(900, 480));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setText("Borrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 210, 100, 25));

        jButton3.setText("Buscar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 90, 100, 25));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel1.setText("Borrar vehículo");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("DNI cliente:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Nombre:");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, -1, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Apellido:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Dominio");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, -1, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Marca del vehículo a eliminar:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText(" modelo del vehículo:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Vehiculo");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 330, -1, -1));
        add(jTextFieldDNI, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 90, 350, -1));

        jTextFieldNombre.setBackground(new java.awt.Color(240, 240, 240));
        jTextFieldNombre.setEnabled(false);
        jTextFieldNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNombreActionPerformed(evt);
            }
        });
        add(jTextFieldNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 130, 370, -1));

        jTextFieldApellido.setBackground(new java.awt.Color(240, 240, 240));
        jTextFieldApellido.setEnabled(false);
        add(jTextFieldApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, 370, -1));
        add(jTextFieldDominio, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 210, 370, -1));

        jTextFieldMarca.setEnabled(false);
        add(jTextFieldMarca, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 250, 240, -1));

        jTextFieldModelo.setEnabled(false);
        add(jTextFieldModelo, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 290, 240, 20));

        jButton2.setText("Buscar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 210, -1, -1));

        jTextFieldVehiculo.setEnabled(false);
        add(jTextFieldVehiculo, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 330, 240, -1));

        jTablePatente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTablePatente);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 260, 410, 220));
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNombreActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        //muestra el mensaje
        int result = JOptionPane.showConfirmDialog(null,"Si para Confirmar","Cliente",dialogButton);
        //pregunta si rusult == si
        if(result == 0){
        try{
        Playa playa = new Playa();
        int dni = Integer.parseInt(jTextFieldDNI.getText());
        int longitud = jTextFieldDNI.getText().length();
        playa.VerificarDocumentos(dni, longitud);
        nuevo = playa.BuscarCliente(dni);
        jTextFieldNombre.setText(nuevo.getNombre());
        jTextFieldApellido.setText(nuevo.getApellido());
        mostrarPatente();
        }catch(ListaVaciaExcepcion e){
            JOptionPane.showMessageDialog(null,"Lista Vacia");
        }catch(DniNoExisteExcepcion ex){
            JOptionPane.showMessageDialog(null,"No se encuentra El Cliente Ingresado pruebe con otro D-N-I");
        }   catch (DocumentoInvalidoExcepcion ex) {
                JOptionPane.showMessageDialog(null,"Ingrese Un Documento Valido");
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(null,"Falta llenar campos");
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        //muestra el mensaje
        int result = JOptionPane.showConfirmDialog(null,"Si para Confirmar","Cliente",dialogButton);
        //pregunta si rusult == si
        if(result == 0){
        try{
        Playa playa = new Playa();
        int dni = Integer.parseInt(jTextFieldDNI.getText());
        nuevo = playa.BuscarCliente(dni);
        Vehiculo vehiculo = new Vehiculo(jTextFieldDominio.getText(),jTextFieldModelo.getText(),jTextFieldMarca.getText(),jTextFieldVehiculo.getText());
        //nuevo.EliminarVehiculo(vehiculo);
        nuevo.eliminarVehiculo(vehiculo.getDominio());
        //nuevo.modificarVehiculo(vehiculo);
        //}catch(DominioNoExisteExcepcion e){
        mostrarPatente();
        JOptionPane.showMessageDialog(null,"El Vehiculo Fue Eliminado");
        //}catch (ListaVaciaExcepcion ex) {
        //JOptionPane.showMessageDialog(null,"Lista Vacia");
        }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(null,"Falta llenar campos");
            } catch (VehiculoNoExisteExcepcion ex) {
                JOptionPane.showMessageDialog(null,"El Vehiculo no Existe");
            } catch (ListaVaciaExcepcion ex) {
                Logger.getLogger(BorrarVehiculo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DniNoExisteExcepcion ex) {
                Logger.getLogger(BorrarVehiculo.class.getName()).log(Level.SEVERE, null, ex);
            }
    }    
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        //muestra el mensaje
        int result = JOptionPane.showConfirmDialog(null,"Si para Confirmar","Cliente",dialogButton);
        //pregunta si rusult == si
        if(result == 0){
        try{
        Playa playa = new Playa();
        String dominio = jTextFieldDominio.getText().toLowerCase();
        playa.VerificarDominio(dominio);
        Vehiculo vehiculo = nuevo.getVehiculo(dominio);
                //nuevo.BuscarVehiculo(dominio);
        jTextFieldMarca.setText(vehiculo.getMarca());
        jTextFieldModelo.setText(vehiculo.getModelo());
        jTextFieldVehiculo.setText(vehiculo.getTipo());
        }catch(DoominioInvalidoExcepcion e){
            JOptionPane.showMessageDialog(null,"Ingrese Un Dominio Valido");
        }catch(NumberFormatException e){
            JOptionPane.showMessageDialog(null,"Falta llenar un campo");
        //} catch (ListaVaciaExcepcion ex) {
          //  JOptionPane.showMessageDialog(null,"Lista Vacia");
        }   catch (VehiculoNoExisteExcepcion ex) {
               JOptionPane.showMessageDialog(null,"El vehiculo no existe");
            }
    }
    }//GEN-LAST:event_jButton2ActionPerformed

    public void mostrarPatente() throws ListaVaciaExcepcion{
        Playa playa = new Playa();
        listar = nuevo.ListaVehiculo();
        

        String matriz[][] = new String[listar.size()][1];
        String guardando;
        for (int i =0;i<listar.size();i++){
            matriz[i][0]=listar.get(i).getDominio();
        }
        jTablePatente.setModel(new javax.swing.table.DefaultTableModel(
            matriz,
            new String [] {
                "Patente"
            }
        ));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTablePatente;
    private javax.swing.JTextField jTextFieldApellido;
    private javax.swing.JTextField jTextFieldDNI;
    private javax.swing.JTextField jTextFieldDominio;
    private javax.swing.JTextField jTextFieldMarca;
    private javax.swing.JTextField jTextFieldModelo;
    private javax.swing.JTextField jTextFieldNombre;
    private javax.swing.JTextField jTextFieldVehiculo;
    // End of variables declaration//GEN-END:variables
}
