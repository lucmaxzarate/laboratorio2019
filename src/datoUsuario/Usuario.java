/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datoUsuario;

/**
 *
 * @author LautaroET
 */
public class Usuario {
    private String codigo;
    private String clave;
    private String tipo;
    private Persona persona;

    public Usuario(String codigo, String clave, String tipo, Persona persona) {
        this.codigo = codigo;
        this.clave = clave;
        this.tipo = tipo;
        this.persona = persona;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    
}
