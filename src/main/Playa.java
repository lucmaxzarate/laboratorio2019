package main;


import Excepcion.AbonoNegativoException;
import Excepcion.TicketExisteException;
import Excepcion.ParametroNoExisteException;
import Excepcion.ParametroExisteException;
import Excepcion.TicketNoExisteException;
import Excepcion.ListaVaciaExcepcion;
import Excepcion.DocumentoInvalidoExcepcion;
import Excepcion.DniNoExisteExcepcion;
import Excepcion.DoominioInvalidoExcepcion;
import Excepcion.DniExisteExcepcion;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joel
 */
public class Playa {
    private static LinkedList<Parametro> parametro = new LinkedList<Parametro>();
    private static LinkedList<Cliente> clientes = new LinkedList<Cliente>();
    private static LinkedList<Ticket> ticket = new LinkedList<Ticket>();
    private static LinkedList<Abono> abono = new LinkedList<Abono>();
    
    
    public void VerificarDocumentos(long documento,int longitud) throws DocumentoInvalidoExcepcion
    {
        if(documento<1 || documento>99999999 || longitud!=8)
        {
           throw new DocumentoInvalidoExcepcion();
        }
    }
    
    public void VerificarDominio(String patente) throws DoominioInvalidoExcepcion
    {
        Pattern p = Pattern.compile("[a-z]{2}[0-9]{3}[a-z]{2}"); //Formato de la patente, 3 letras, espacio y 3 números
        Matcher m = p.matcher(patente);
        if (!m.matches()) {
        //no es una patente
        throw new DoominioInvalidoExcepcion();
        }
    }
    
    /*public void verificarLetra(String nombre) throws DoominioInvalidoExcepcion
    {
        Pattern p = Pattern.compile("{3}>[a-z]<{20}"); //Formato de la patente, 3 letras, espacio y 3 números
        Matcher m = p.matcher(nombre);
        if (!m.matches()) {
 
        throw new DoominioInvalidoExcepcion();
        }
        }
    */
    public void agregarAbono(Abono nuevo){
        abono.add(nuevo);
    }
    
    public int numeroAbono(){
        int num = 0;
        num=abono.size();
        return num;
       
    }
    
    public LinkedList listaAbono() throws ListaVaciaExcepcion {
        if(abono.size()==0){
             throw new ListaVaciaExcepcion();
         } 
        return abono;
     }
    public Abono buscarAbono(int dni) throws ListaVaciaExcepcion, DniNoExisteExcepcion{
        listaAbono();
        for(int i=0;i<abono.size();i++)
            {
                if(abono.get(i).getDni()==dni)
                {
                   return abono.get(i);
                }
            }
        throw new DniNoExisteExcepcion();
    }
    
    public LinkedList ListaCliente() throws ListaVaciaExcepcion{
         if(clientes.size()==0){
             throw new ListaVaciaExcepcion();
         }
         return clientes;
     }
    
    public void IngresarCliente(Cliente nuevo) throws DniExisteExcepcion{
        if(clientes.size()>0)
        {
            for(int i=0;i<clientes.size();i++)
            {
                if(clientes.get(i).getDni()==nuevo.getDni())
                {
                    throw new DniExisteExcepcion();
                }
            } 
        }

            clientes.add(nuevo);
    }
    
    public Cliente getCliente(int dni) throws DniNoExisteExcepcion{
        Cliente clienteEncontrado = null;
        for (Cliente var : clientes) {
            if (var.getDni()==dni) {
                clienteEncontrado = var;
                break;
            }
        }
        if (clienteEncontrado == null) {
            throw new DniNoExisteExcepcion();
        }
        return clienteEncontrado;
    }

    public void modificarCliente(Cliente nuevo) throws DniNoExisteExcepcion {
        Cliente clienteEncontrado;
        clienteEncontrado = getCliente(nuevo.getDni());
        clientes.remove(clienteEncontrado); 
        clientes.add(nuevo);               
	}
    
    /*
    public void ModificarCliente(Cliente nuevo) throws ListaVaciaExcepcion, DniNoExisteExcepcion{
        ListaCliente();
        if(clientes.size()>0)
        {
            for(int i=0;i<clientes.size();i++)
            {
                if(clientes.get(i).getDni()== nuevo.getDni());
                {
                    clientes.set(i, nuevo);
                }  
            } 
        }else{
            throw new DniNoExisteExcepcion();
        }
    }*/
    
    public void EliminarCliente(Cliente nuevo) throws ListaVaciaExcepcion, DniNoExisteExcepcion{
        ListaCliente();
        boolean bandera = false;
        if(clientes.size()>0)
        {
            for(int i=0;i<clientes.size();i++)
            {
                if(clientes.get(i).getDni()==nuevo.getDni())
                {
                    clientes.remove(clientes.get(i));
                    bandera = true;
                }
            }
        }
        if(bandera==false)
        {
            throw new DniNoExisteExcepcion(); 
        }
    }
    
    public Cliente BuscarCliente(int dni) throws ListaVaciaExcepcion, DniNoExisteExcepcion{
        ListaCliente();
        for(int i=0;i<clientes.size();i++)
            {
                if(clientes.get(i).getDni()==dni)
                {
                   return clientes.get(i);
                }
            }
        throw new DniNoExisteExcepcion();
    }

    
    
    
    
    
    
    public LinkedList ListaTictek() throws ListaVaciaExcepcion{
        if(ticket.size()==0){ 
            throw new ListaVaciaExcepcion();
         }
         return ticket;
        
    }
    public int NumeroIdentificacion(){
        int num = 0;
        num=ticket.size();
        return num;
       
    }
    
    public void ingresarTicket(Ticket nuevo) throws TicketExisteException{
        if(clientes.size()>0)
        {
            for(int i=0;i<ticket.size();i++)
            {
                if(ticket.get(i).getFecha().equals(nuevo.getFecha()));
                {
                    if(ticket.get(i).getCliente().getDni()==nuevo.getCliente().getDni()){
                       throw new TicketExisteException(); 
                    }
                }
            } 
        }

            ticket.add(nuevo);
    }
    /*public void agregarTicket(Ticket nuevo) throws TicketExisteException {
        for (Ticket var : ticket) {
            if (var.getFecha().equals(nuevo.getFecha())) {
                throw new TicketExisteException();
            }        
        }
        ticket.add(nuevo);
    }*/
    

    public Ticket getTicket(int dni,String fecha) throws TicketNoExisteException{
        Ticket ticketEncontrado = null;
        for (Ticket var : ticket) {
            if (var.getFecha().equals(fecha)) {
                if(var.getCliente().getDni() == dni){
                    ticketEncontrado = var;
                    break;  
                }

            }
        }
        if (ticketEncontrado == null) {
            throw new TicketNoExisteException();
        }
        return ticketEncontrado;
    }
    public void modificarTicket(Ticket nuevo) throws TicketNoExisteException {
        Ticket ticketEncontrado;
        ticketEncontrado = getTicket(nuevo.getCliente().getDni(),nuevo.getFecha());
        ticket.remove(ticketEncontrado); 
        ticket.add(nuevo);               
	}
    
    
    public void agregarParametro(Parametro nuevo) throws ParametroExisteException {
        for (Parametro var : parametro) {
            if (var.equals(nuevo)) {
                throw new ParametroExisteException();
            }        
        }
        parametro.add(nuevo);
    }
    
    public void eliminarParametro(int codigo) throws ParametroNoExisteException {
        Parametro parametroEncontrado = getParametro(codigo);
        parametro.remove(parametroEncontrado);
    }

    public Parametro getParametro(int codigo) throws ParametroNoExisteException{
        Parametro parametroEncontrado = null;
        for (Parametro var : parametro) {
            if (var.getCodigo()==codigo) {
                parametroEncontrado = var;
                break;
            }
        }
        if (parametroEncontrado == null) {
            throw new ParametroNoExisteException();
        }
        return parametroEncontrado;
    }

    public void modificarParametro(Parametro nuevo) throws ParametroNoExisteException {
        Parametro parametroEncontrado;
        parametroEncontrado = getParametro(nuevo.getCodigo());
        parametro.remove(parametroEncontrado); 
        parametro.add(nuevo);               
	}
    public LinkedList ListaParametro(){
         return clientes;
     }
    public float CalcularCredito(float abono,float precio,float descuento){
        float num, num1;
        num1=abono-precio;
        num = num1+((precio*descuento)/100);
        return num;
        
    }
    public float Calcular(float x,float y){
        float result = x+y;
        return result;
    }

    public int saldoNegativo(float abono,int negativo) throws AbonoNegativoException{
        if (abono<0){
            if(negativo==2){
                throw new AbonoNegativoException();
            }else {
                return negativo+1;
            }
        }
        return 0;
        
    }
    public static LinkedList<Parametro> getParametro() {
        return parametro;
    }

    public static void setParametro(LinkedList<Parametro> parametro) {
        Playa.parametro = parametro;
    }

    public static LinkedList<Cliente> getClientes() {
        return clientes;
    }

    public static void setClientes(LinkedList<Cliente> clientes) {
        Playa.clientes = clientes;
    }

    public static LinkedList<Ticket> getTicket() {
        return ticket;
    }

    public static void setTicket(LinkedList<Ticket> ticket) {
        Playa.ticket = ticket;
    }

    

   
  
    
    
}
