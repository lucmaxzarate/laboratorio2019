/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Excepcion.AbonoNegativoException;
import Excepcion.DocumentoInvalidoExcepcion;
import Excepcion.DoominioInvalidoExcepcion;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author joel
 */
public class Dato {
    
    public static void VerificarDocumentos(long documento,int longitud) throws DocumentoInvalidoExcepcion
    {
        if(documento<1 || documento>99999999 || longitud!=8)
        {
           throw new DocumentoInvalidoExcepcion();
        }
    }
    
    public static void VerificarDominio(String patente) throws DoominioInvalidoExcepcion
    {
        Pattern p = Pattern.compile("[a-z]{2}[0-9]{3}[a-z]{2}"); //Formato de la patente, 3 letras, espacio y 3 números
        Matcher m = p.matcher(patente);
        if (!m.matches()) {
        //no es una patente
        throw new DoominioInvalidoExcepcion();
        }
    }
    
    
    
    public static float CalcularCredito(float abono,float precio,float descuento){
        float num, num1;
        num1=abono-precio;
        num = num1+((precio*descuento)/100);
        return num;
        
    }
    
    public static float Calcular(float x,float y){
        float result = x+y;
        return result;
    }

    public static int saldoNegativo(float abono,int negativo) throws AbonoNegativoException{
        if (abono<0){
            if(negativo==2){
                throw new AbonoNegativoException();
            }else {
                return negativo+1;
            }
        }
        return 0;
        
    }
}
