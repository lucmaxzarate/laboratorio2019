package main;


import Excepcion.DominioExisteExcepcion;
import Excepcion.ListaVaciaExcepcion;
import Excepcion.VehiculoNoExisteExcepcion;
import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joel
 */
public class Cliente {
    private String nombre;
    private String apellido;
    private int dni;
    private float abono;
    private LinkedList<Vehiculo> vehiculo = new LinkedList<Vehiculo>();

    public Cliente(String nombre, String apellido, int dni) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
    }

    
    public void IngresarVehiculo(Vehiculo nuevo) throws DominioExisteExcepcion{
        if(vehiculo.size()>0)
        {
            for(int i=0;i<vehiculo.size();i++)
            {
                if(this.vehiculo.get(i).getDominio().equals(nuevo.getDominio()))
                {
                    throw new DominioExisteExcepcion();
                }
            } 
        }
        vehiculo.add(nuevo);
    }
    
    public void modificarVehiculo(Vehiculo nuevo) throws VehiculoNoExisteExcepcion {
        Vehiculo vehiculoEncontrado;
        vehiculoEncontrado = getVehiculo(nuevo.getDominio());
        vehiculo.remove(vehiculoEncontrado); 
        vehiculo.add(nuevo);               
	}
    /*public void ModificarVehiculo(Vehiculo nuevo) throws DominioNoExisteExcepcion, ListaVaciaExcepcion{
        ListaVehiculo();
        if(vehiculo.size()>0)
        {
            for(int i=0;i<vehiculo.size();i++)
            {
                if(vehiculo.get(i).getDominio().equals(nuevo.getDominio()));
                {
                    vehiculo.set(i, nuevo);
                }  
            } 
        }else{
            throw new DominioNoExisteExcepcion();
        }
    }*/
    public void eliminarVehiculo(String dominio) throws VehiculoNoExisteExcepcion {
        Vehiculo vehiculoEncontrado = getVehiculo(dominio);
        vehiculo.remove(vehiculoEncontrado);
    }
    
    public Vehiculo getVehiculo(String dominio) throws VehiculoNoExisteExcepcion{
        Vehiculo vehiculoEncontrado = null;
        for (Vehiculo var : vehiculo) {
            if (var.getDominio().equals(dominio)) {
                vehiculoEncontrado = var;
                break;
            }
        }
        if (vehiculoEncontrado == null) {
            throw new VehiculoNoExisteExcepcion();
        }
        return vehiculoEncontrado;
    }/*
    public void EliminarVehiculo(Vehiculo nuevo) throws DominioNoExisteExcepcion, ListaVaciaExcepcion{
        ListaVehiculo();
        boolean bandera = false;
        if(vehiculo.size()>0)
        {
            for(int i=0;i<vehiculo.size();i++)
            {
                if(vehiculo.get(i).getDominio().equals(nuevo.getDominio()));
                {
                    vehiculo.remove(vehiculo.get(i));
                    bandera = true;
                }
            }
        }
        if(bandera==false)
        {
            throw new DominioNoExisteExcepcion();
        }
    }
    
    public Vehiculo BuscarVehiculo(String dominio) throws VehiculoNoExisteExcepcion, ListaVaciaExcepcion{
        ListaVehiculo();
        for(int i=0;i<vehiculo.size();i++)
            {
                if(vehiculo.get(i).getDominio().equals(dominio));
                {
                   return vehiculo.get(i);
                }
            }
        throw new VehiculoNoExisteExcepcion();
       
    }*/
    public LinkedList ListaVehiculo() throws ListaVaciaExcepcion{
        if(vehiculo.size()==0){
             throw new ListaVaciaExcepcion();
         }
        return vehiculo;
    }
   
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public float getAbono() {
        return abono;
    }

    public void setAbono(float abono) {
        this.abono = abono;
    }

    public LinkedList<Vehiculo> getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(LinkedList<Vehiculo> vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    
}
