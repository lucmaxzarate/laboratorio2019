package main;

import datoUsuario.Usuario;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;

public  class Ticket{
private Cliente cliente;
private Float montoDisponible;
private Float montoacreditado;
private LinkedList<Vehiculo> dominio;
private String fecha;
private String hora;
private int Identificacion;
private int saldoNegativo;
private LinkedList<IngresoDia> ingresoDia = new LinkedList<IngresoDia>();
;
private String tipo;

    public Ticket(Cliente cliente, Float montoDisponible, Float montoacreditado, String fecha, String hora, int Identificacion) {
        this.cliente = cliente;
        this.montoDisponible = montoDisponible;
        this.montoacreditado = montoacreditado;
        this.fecha = fecha;
        this.hora = hora;
        this.Identificacion = Identificacion;
    }
    
    public void agregarIngresoDia(IngresoDia nuevo) {
        ingresoDia.add(nuevo);
    }
    
    public int calcularIngreso(){
        int num = 0;
        num=ingresoDia.size();
        return num+1;
       
    }
    
    
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Float getMontoDisponible() {
        return montoDisponible;
    }

    public void setMontoDisponible(Float montoDisponible) {
        this.montoDisponible = montoDisponible;
    }

    public Float getMontoacreditado() {
        return montoacreditado;
    }

    public void setMontoacreditado(Float montoacreditado) {
        this.montoacreditado = montoacreditado;
    }

    public LinkedList<Vehiculo> getDominio() {
        return dominio;
    }

    public void setDominio(LinkedList<Vehiculo> dominio) {
        this.dominio = dominio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(int Identificacion) {
        this.Identificacion = Identificacion;
    }

    public int getSaldoNegativo() {
        return saldoNegativo;
    }

    public void setSaldoNegativo(int saldoNegativo) {
        this.saldoNegativo = saldoNegativo;
    }


    public LinkedList<IngresoDia> getIngresoDia() {
        return ingresoDia;
    }

    public void setIngresoDia(LinkedList<IngresoDia> ingresoDia) {
        this.ingresoDia = ingresoDia;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    

 
    



}