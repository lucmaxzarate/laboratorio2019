/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author joel
 */
public class IngresoDia {
    private String fecha;
    private String hora;
    private String observacion;
    private String dominio;
    private float valoringreso;
    private float saldoDisponible;
    private boolean estado;

    public IngresoDia(String fecha, String hora, String dominio, float valoringreso, float saldoDisponible) {
        this.fecha = fecha;
        this.hora = hora;
        this.dominio = dominio;
        this.valoringreso = valoringreso;
        this.saldoDisponible = saldoDisponible;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getDominio() {
        return dominio;
    }

    public void setDominio(String dominio) {
        this.dominio = dominio;
    }

    public float getValoringreso() {
        return valoringreso;
    }

    public void setValoringreso(float valoringreso) {
        this.valoringreso = valoringreso;
    }

    public float getSaldoDisponible() {
        return saldoDisponible;
    }

    public void setSaldoDisponible(float saldoDisponible) {
        this.saldoDisponible = saldoDisponible;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    
    
    
    
}
