/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Excepcion.TicketExisteException;
import Excepcion.DniNoExisteExcepcion;
import Excepcion.ParametroNoExisteException;
import Excepcion.TicketNoExisteException;
import Excepcion.DniExisteExcepcion;
import Excepcion.ListaVaciaExcepcion;
import Excepcion.AbonoNegativoException;
import Excepcion.ParametroExisteException;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel
 */
public class PlayaTest {
    
    public PlayaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of ListaCliente method, of class Playa.
     */
    @Test
    public void testListaCliente() {
        //System.out.println("ListaCliente");
        //Playa instance = new Playa();
        //LinkedList expResult = null;
        //LinkedList result = instance.ListaCliente();
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of IngresarCliente method, of class Playa.
     */
    @Test
    public void testIngresarCliente() throws DniExisteExcepcion {
        System.out.println("IngresarCliente");
        Playa playa = new Playa();    
        Cliente c1 = new Cliente("Lautaro","Tapia",10);;
        playa.IngresarCliente(c1);
        assertEquals(4,playa.getClientes().size());
    }
    
    @Test (expected = DniExisteExcepcion.class)
    public void testIngresarClienteExiste() throws DniExisteExcepcion {
        System.out.println("IngresarCliente");
        Playa playa = new Playa();    
        Cliente c1 = new Cliente("Lautaro","Tapia",01234567);;
        playa.IngresarCliente(c1);
        Cliente c2 = new Cliente("Lautaro","Tapia",01234567);
        playa.IngresarCliente(c2);
    }

    /**
     * Test of ModificarCliente method, of class Playa.
     */
    @Test
    public void testModificarCliente() throws DniExisteExcepcion, ListaVaciaExcepcion, DniNoExisteExcepcion {
        System.out.println("ModificarCliente");
        Playa playa = new Playa();    
        Cliente c1 = new Cliente("Lautaro","Tapia",12345678);;
        playa.IngresarCliente(c1);
        Cliente c2 = new Cliente("Nelson","Romeo",12345678);         
        //playa.ModificarCliente(c2);
        playa.modificarCliente(c2);
        
        assertEquals("Nelson",playa.BuscarCliente(12345678).getNombre());
    }

    /**
     * Test of EliminarCliente method, of class Playa.
     */
    @Test
    public void testEliminarCliente() throws DniExisteExcepcion, ListaVaciaExcepcion, DniNoExisteExcepcion {
        System.out.println("EliminarCliente");
        Playa playa = new Playa();    
        Cliente c1 = new Cliente("Lautaro","Tapia",12345678);;
        playa.IngresarCliente(c1);
        playa.EliminarCliente(c1);
        assertEquals(4,playa.getClientes().size());
    }

    /**
     * Test of BuscarCliente method, of class Playa.
     */
    @Test
    public void testBuscarCliente() throws DniExisteExcepcion, ListaVaciaExcepcion, DniNoExisteExcepcion {
        System.out.println("BuscarCliente");
        Playa playa = new Playa();    
        Cliente c1 = new Cliente("Lautaro","Tapia",1);;
        playa.IngresarCliente(c1);  
        assertEquals("Lautaro",playa.BuscarCliente(1).getNombre());
    }
    @Test (expected = DniNoExisteExcepcion.class)
    public void testBuscarClienteNoExiste() throws DniExisteExcepcion, ListaVaciaExcepcion, DniNoExisteExcepcion {
       System.out.println("BuscarCliente");
       Playa playa = new Playa();    
       Cliente c1 = new Cliente("Lautaro","Tapia",98765431);
       playa.IngresarCliente(c1);  
       playa.BuscarCliente(98765432);
    }
    
    @Test
    public void IngresoTicket()throws DniExisteExcepcion, ListaVaciaExcepcion, DniNoExisteExcepcion,TicketExisteException, TicketNoExisteException{
        Playa playa = new Playa();    
        Cliente c1 = new Cliente("Lautaro","Tapia",2);
        playa.IngresarCliente(c1);
        Ticket t1 =new Ticket(c1,200.6f, 200.6f, "", "",1); 
        playa.ingresarTicket(t1);
        assertEquals(3,playa.getTicket().size());

    }
    @Test (expected = TicketExisteException.class)
    public void IngresoTicketYaExiste()throws DniExisteExcepcion, ListaVaciaExcepcion, DniNoExisteExcepcion,TicketExisteException, TicketNoExisteException{
        Playa playa = new Playa();    
        Cliente c1 = new Cliente("Lautaro","Tapia",78945612);
        Cliente c2 = new Cliente("Lautaro","Tapia",78945612);
        playa.IngresarCliente(c1);
        Ticket t1 =new Ticket(c1,200.6f, 200.6f, "", "",1);
        Ticket t2 =new Ticket(c2,200.6f, 200.6f, "", "",1); 
        playa.ingresarTicket(t1);
        playa.ingresarTicket(t2);

    }
    
    @Test
    public void BuscarTicket()throws DniExisteExcepcion, ListaVaciaExcepcion, DniNoExisteExcepcion,TicketExisteException, TicketNoExisteException{
       Playa playa = new Playa();    
        Cliente c1 = new Cliente("Lucas","Zarate",3);
        playa.IngresarCliente(c1);
        Ticket t1 =new Ticket(c1,200.6f, 200.6f, "", "",1); 
        playa.ingresarTicket(t1);
        playa.getTicket(3,"");
        assertEquals(3,t1.getCliente().getDni()); 
    }
    
    @Test (expected = TicketNoExisteException.class)
    public void BuscarTicketNoExiste()throws DniExisteExcepcion, ListaVaciaExcepcion, DniNoExisteExcepcion,TicketExisteException, TicketNoExisteException{
       Playa playa = new Playa();    
        Cliente c1 = new Cliente("Lucas","Zarate",36547891);
        playa.IngresarCliente(c1);
        Ticket t1 =new Ticket(c1,200.6f, 200.6f, "", "",1); 
        playa.ingresarTicket(t1);
        playa.getTicket(36547890,"05-set-2015");
    }
    @Test
    public void ModificarTicket()throws DniExisteExcepcion, ListaVaciaExcepcion, DniNoExisteExcepcion,TicketExisteException, TicketNoExisteException{
        
        Playa playa = new Playa();    
        Cliente c1 = new Cliente("Lucas","Zarate",4);
        Ticket t1 =new Ticket(c1,200.6f, 200.6f, "", "",2);
        playa.ingresarTicket(t1);
        Ticket t2 =new Ticket(c1,300f, 200.6f, "", "",3);
        playa.modificarTicket(t2);
        assertEquals(3,playa.getTicket(4, "").getIdentificacion());
      
    }
    @Test
    public void agregarParametro() throws ParametroExisteException, ParametroNoExisteException {
        Playa playa=new Playa();
        Parametro p1=new Parametro(1,"", 3f); 
        playa.agregarParametro(p1);
       assertEquals(1,playa.getParametro().size());
        }
    @Test
    public void EliminarParametro() throws ParametroExisteException, ParametroNoExisteException {
        Playa playa=new Playa();
        playa.eliminarParametro(1);
       assertEquals(0,playa.getParametro().size());
        }
    @Test
  public void getParametro() throws ParametroNoExisteException, ParametroExisteException{
  Playa playa=new Playa();
  Parametro p1=new Parametro(2,"", 3f); 
  playa.agregarParametro(p1);
  assertEquals(p1,playa.getParametro(2));
  }
  
  @Test
  public void calcularCredito(){
    Playa playa=new Playa();
    float result;
    result=playa.CalcularCredito(100, 100, 50);
    assertEquals(50,result,0);    
  }
  
  @Test
 public void saldoNegativo() throws AbonoNegativoException{
  Playa playa=new Playa();
  float result=playa.saldoNegativo(100, 0);
  assertEquals(0,result,0);    

 }
  @Test
public void Calcular(){
    Playa playa=new Playa();   
    assertEquals(30,playa.Calcular(10, 20),0);
}  
}