/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Excepcion.VehiculoNoExisteExcepcion;
import Excepcion.DniNoExisteExcepcion;
import Excepcion.DniExisteExcepcion;
import Excepcion.ListaVaciaExcepcion;
import Excepcion.DominioExisteExcepcion;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alumno
 */
public class ClienteTest {
    
    public ClienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
@Test
public void Ingresarvehiculo() throws DniExisteExcepcion, DominioExisteExcepcion, DniNoExisteExcepcion{
Playa playa=new Playa();
Cliente c1=new Cliente("Lucas","Zarate",1);
playa.IngresarCliente(c1);
Vehiculo v1 =new Vehiculo("1","marca","modelo","auto");
c1.IngresarVehiculo(v1);
assertEquals(1,playa.getCliente(1).getVehiculo().size());
}
@Test (expected = DominioExisteExcepcion.class)
public void testingresarVehiculoYaExiste() throws DniExisteExcepcion, DominioExisteExcepcion, DniNoExisteExcepcion{
Playa playa=new Playa();
Cliente c1=new Cliente("Lucas","Zarate",35390795);
playa.IngresarCliente(c1);
Vehiculo v1 =new Vehiculo("1","marca","modelo","auto");
Vehiculo v2 =new Vehiculo("1","marca","modelo","auto");
c1.IngresarVehiculo(v1);
c1.IngresarVehiculo(v2);
}

@Test
public void Modificarvehiculo() throws DniExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion, VehiculoNoExisteExcepcion, ListaVaciaExcepcion, DniNoExisteExcepcion{
Playa playa=new Playa();
Cliente c1=new Cliente("Lucas","Zarate",2);
playa.IngresarCliente(c1);
Vehiculo v1 =new Vehiculo("1","marca","modelo","auto");
c1.IngresarVehiculo(v1);    
Vehiculo v2 =new Vehiculo("1","marcamodificada","modelo","auto");
c1.modificarVehiculo(v2);
assertEquals("marcamodificada",playa.getCliente(2).getVehiculo("1").getMarca());
}

@Test (expected = VehiculoNoExisteExcepcion.class)
public void testmodificarVehiculoNoExisteDominio() throws DniExisteExcepcion, DominioExisteExcepcion, VehiculoNoExisteExcepcion{
Playa playa=new Playa();
Cliente c1=new Cliente("Lucas","Zarate",1523467);
playa.IngresarCliente(c1);
Vehiculo v1 =new Vehiculo("1","marca","modelo","auto");
c1.IngresarVehiculo(v1);    
Vehiculo v2 =new Vehiculo("lauty","marcamodificada","modelo","auto");
c1.modificarVehiculo(v2);
}

@Test
public void Eliminarvehiculo() throws VehiculoNoExisteExcepcion, DniNoExisteExcepcion, DominioExisteExcepcion, DniExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion, DominioExisteExcepcion{
Playa playa=new Playa();
Cliente c1=new Cliente("Lucas","Zarate",16849816);
playa.IngresarCliente(c1);
Vehiculo v1 =new Vehiculo("168","marca","modelo","auto");
c1.IngresarVehiculo(v1);
c1.eliminarVehiculo("168");
assertEquals(0,playa.getCliente(16849816).getVehiculo().size());

}

@Test (expected = VehiculoNoExisteExcepcion.class)
public void testeliminarVehiculoNoExiste() throws DniExisteExcepcion, VehiculoNoExisteExcepcion, DominioExisteExcepcion {
Playa playa=new Playa();
Cliente c1=new Cliente("Lucas","Zarate",12345612);
playa.IngresarCliente(c1);
Vehiculo v1 =new Vehiculo("1","marca","modelo","auto");
c1.IngresarVehiculo(v1);
c1.eliminarVehiculo("2");
}

}
